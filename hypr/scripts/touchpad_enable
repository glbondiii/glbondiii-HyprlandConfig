#!/bin/sh

# Source: https://github.com/dianaw353/dotfiles/blob/main/roles/hyprland/files/hypr/scripts/touchpad_toggle.sh
#
# Script to enable the touchpad
# Use the following command to choose what device you want to toggle:
#   hyprctl devices
#
# Courtesy of r/hyprland Reddit community:
#   https://reddit.com/r/hyprland/comments/11kr8bl/hotkey_disable_touchpad/
#   https://reddit.com/r/hyprland/comments/1bqohmd/dynamically_enabledisable_device/
#
# Ported, generalized, improved and overengineered by Bahar Kurt for
# Diana's dotfiles.
#

# HACK: Try to set a nonexistent config under "device:" so that
# Hyprland refreshes all properties inside.
hyprctl keyword device:a true > /dev/null 2>&1

# Set device to be toggled
export HYPRLAND_TP_DEVICE="$(hyprctl devices | grep touchpad | sed '/2-synaptics-touchpad/d; s/.*	//')"
export HYPRLAND_TP_VARIABLE="device[${HYPRLAND_TP_DEVICE}]:enabled"

if [ -z "$XDG_RUNTIME_DIR" ]; then
  export XDG_RUNTIME_DIR=/run/user/$(id -u)
fi

# Check if device is currently enabled
export TP_STATUS_FILE="$XDG_RUNTIME_DIR/touchpad.status"

# Try to get the touchpad status from status file.
if [ -f "$TP_STATUS_FILE" ]; then
  export TOUCHPAD_ENABLED="$(cat "$TP_STATUS_FILE")"
fi

if [ "$TOUCHPAD_ENABLED" != "true" ]; then
  export TP_PREVIOUS_STATUS="false"
  export TOUCHPAD_ENABLED="true"
  # Try to enable the touchpad. If it fails, set the new status to disabled.
  hyprctl --batch -r -- keyword "$HYPRLAND_TP_VARIABLE" $TOUCHPAD_ENABLED || export TOUCHPAD_ENABLED="false"

  # Write the new touchpad status into the status file.
  echo "$TOUCHPAD_ENABLED" > "$TP_STATUS_FILE"

  # Generate the notification message.
  export TP_NOTIFMSG="Touchpad "

  if [ "$TOUCHPAD_ENABLED" == "$PREVIOUS_STATUS" ]; then
    export TP_NOTIFMSG+="could not be "
    # Touchpad could not be...
  fi

  export TP_NOTIFMSG+="enabled."
  # Touchpad (could not be) enabled.

  notify-send -u normal "$TP_NOTIFMSG"
fi
