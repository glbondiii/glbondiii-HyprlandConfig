#!/bin/sh

ln -s "$(pwd)/hypr" ~/.config
ln -s "$(pwd)/waybar" ~/.config
ln -s "$(pwd)/dunst" ~/.config
ln -s "$(pwd)/rofi" ~/.config
ln -s "$(pwd)/fcitx5" ~/.config

echo "Hyprland config installed"

