#!/bin/sh

ln -s "$(pwd)/hypr" ~/.config
ln -s "$(pwd)/waybar" ~/.config
ln -s "$(pwd)/dunst" ~/.config
ln -s "$(pwd)/rofi" ~/.config
echo "Hyprland config installed"

