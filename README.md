# NaCl-HyprlandConfig 
My Hyprland config (also includes dunst, waybar, and rofi configs, as well as scripts for
brightness, volume, and sleep, plus some wallpapers and rofi themes)  
## Install: 
Clone this repo into in your home directory, enter the repo directory, and run INSTALL.sh  
* (note: be sure to delete or backup your existing Hyprland config; deleting hyprland.conf will crash Hyprland, so run 
the install script either in a separate DE or in a tty)

## Special Thanks:
* newmanls for bundled rofi themes (Source: [rofi-themes-collection](https://github.com/newmanls/rofi-themes-collection/blob/master/screenshots/launchpad.png))
